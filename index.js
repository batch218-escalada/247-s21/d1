console.log('Hello World');

// [SECTION] Arrays

    // Arrays are used to store multiple related values in a single variable.
    // There are declared using square brackets ([]) also known as 'Array Literals'.

    /* 
        Syntax:
            let/const arrayName = [elementA, elementB, elementC, ...];
    */


    let grades = [98.5, 94.3, 89.2, 90.1];
    let computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu'];

    console.log(grades);
    // Result ---> (8) ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu']

    console.log(computerBrands);

    // Possible use of an array but is not recommended.
    let mixedArr = [12, 'Asus', null, undefined, {}];

    console.log(mixedArr);

    // Alternative way to write arrays

    let myTasks = [
        'drink html',
        'eat javascript',
        'inhale css',
        'bake sass',
    ];

    // Result ---> (4) ['drink html', 'eat javascript', 'inhale css', 'bake sass']

    console.log(myTasks);

    let city1 = 'Tokyo';
    let city2 = 'Manila';
    let city3 = 'Jakarta';

    let cities = [city1, city2, city3];

    console.log(cities);

// [SECTION] Length Property

    // The .length property allows us to get the set the total number of items in an array.

    console.log(myTasks.length);    /* Result ---> 4 */

    console.log(cities.length);     /* Result ---> 3 */

    let blankArr = [];
    console.log(blankArr.length);   /* Result ---> 0 */

    let fullName = 'Sir Carlo';
    // length property can also be used with strings.
    console.log(fullName.length);   /* Result ---> 9, is not an array because there's no square bracket. */

    myTasks.length = myTasks.length - 1;
    // Length property can also set the total number of items in an array. Meaning we can actually delete the last item in the array or shorten the array simply by adding the length property of an array.

    console.log(myTasks.length);    /* Result ---> 3 */

    console.log(myTasks);           /* Result ---> (3) ['drink html', 'eat javascript', 'inhale css'] */

    // Another example using decrementation
    cities.length--;
    console.log(cities);

    let theBeatles = ['John', 'Ringo', 'Paul', 'George'];
    theBeatles.length++;
    console.log(theBeatles); /* Result ---> (5) ['John', 'Ringo', 'Paul', 'George', empty] */

// [SECTION] Readubg from Arrays

    //  Accessing array elements is one of the more common tasks we do with an array.

    /* 
        Syntax:
            arrayName[index];
    */

    console.log(grades[3]);         /* Result ---> 90.1 */
    console.log(computerBrands[2]); /* Result ---> Lenovo */
    console.log(grades[20]);        /* Result ---> undefined */

    let lakersLegend = ['Kobe', 'Shaq', 'Lebron', 'Magic', 'Kareem'];
    console.log(lakersLegend[1]);   /* Result ---> Shaq */
    console.log(lakersLegend[3]);   /* Result ---> Magic */

    let currentLaker = lakersLegend[2];
    console.log(currentLaker);      /* Result ---> Lebron */

    // We can also reassign array values using the items' indices

    console.log('Array before reassignment');
    console.log(lakersLegend);
    lakersLegend[2] = 'Pau Gasol';
    console.log('Array after reassignment');
    console.log(lakersLegend);      /* Result ---> (5) ['Kobe', 'Shaq', 'Pau Gasol', 'Magic', 'Kareem'] */
    
    // [SUB-SECTION] Accessing the last element of an array

    let bullsLegends = ['Jordan', 'Pippen', 'Rodman', 'Rose', 'Kukoc'];

    let lastElementIndex = bullsLegends.length - 1;

    console.log(bullsLegends[lastElementIndex]);            /* Result ---> Kukoc */

        // We can also add it directly

        console.log(bullsLegends[bullsLegends.length - 1]); /* Result ---> Kukoc */

    // [SUB-SECTION] Adding items into the array

        let newArr = [];
        console.log(newArr[0]);

        newArr[0] = 'Cloud Strife';
        console.log(newArr);

        newArr[1] = 'Tifa Lockhart';
        console.log(newArr);                        /* Result ---> (2) ['Cloud Strife', 'Tifa Lockhart'] */

        newArr[newArr.length] = 'Barrett Walace';
        console.log(newArr);                        /* Result ---> (3) ['Cloud Strife', 'Tifa Lockhart', 'Barrett Walace'] */

// [SECTION] Looping over an Array

    // You can use a for loop to iterate over all items in an array.

    for (let index = 0; index < newArr.length; index++) {
        console.log(newArr[index]); 

        /* Result --->  Cloud Strife
                        Tifa Lockhart
                        Barrett Walace 
        */
        
    };

    // Given an array of numbers, you can also show if the follow items in the array are divisible by 5 or not. You can mix in an if-else statment in the loop:

    let numArr = [5, 12, 30, 46, 40];

    for(let index = 0; index < numArr.length; index++){
        
        if (numArr[index] % 5 === 0) {
            console.log(numArr[index] + ' is divisible by 5');
        } else {
            console.log(numArr[index] + ' is not divisible by 5')
        };
    };

// [SECTION] Multidimensional Arrays

    // Multidimensional arrays are useful for storing complex data structures.

    let chessBoard = [
        ['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'h1'],
        ['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'h2'],
        ['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'h3'],
        ['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'h4'],
        ['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'h5'],
        ['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'h6'],
        ['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'h7'],
        ['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'h8']
    ];

    console.log(chessBoard);

    // Accessing elements of a multidimentional arrays
        console.log(chessBoard[1][4]);

        console.log('Pawn moves to: ' + chessBoard[1][4]);